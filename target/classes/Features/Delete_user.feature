#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Delete an user

  @tag1
  Scenario: Delete an user test scenario
    Given The user is on the home page_du
    #And some other precondition
    When The user selects the user menu_du
    #And some other action
    #And yet another action
    Then The current users should be displayed_du
    And The user moves to the last page
    Then The user should see the latest users added
    When The user selects the option to delete the last user
    Then The user is deleted

