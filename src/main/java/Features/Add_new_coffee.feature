#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Add a new coffee

  @tag1
  Scenario: Add new coffee test scenario
    Given The user is on the home page_anc
    #And some other precondition
    When The user selects the coffee menu_anc
    #And some other action
    #And yet another action
    Then The current coffee products should be displayed_anc
    When The user fills the information for a new coffee
    And The user selects the submit option
    Then The new coffee should be added
