#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Add a new user

  @tag1
  Scenario: Add a new user test
    Given The user is on the home page_anu
    #And some other precondition
    When The user selects the user menu_anu
    #And some other action
    #And yet another action
    Then The current users should be displayed_anu
    When The user fills the information for a new user
    And The user selects the submit option_anu
    Then The new user should be added

