package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class baseMethods {

		
	public WebDriver openWebPage(WebDriver driver) {
							
		System.setProperty("webdriver.gecko.driver", "src//main//java//Libraries//geckodriver.exe");	
		driver= new FirefoxDriver();					
        driver.manage().window().maximize();			
        driver.get("http://localhost:8081/");
			
        return driver;
	}
	
	
	public void wait(int seconds) {
		
		try {
			Thread.sleep(seconds*1000);
		}catch(InterruptedException e){}
		
	}
}
