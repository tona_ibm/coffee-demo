package stepDefinitions;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class deleteCoffeeStepDefinition extends baseMethods{
 
	
	WebDriver driver;
	WebDriverWait wait;
	List <WebElement> rows;
	WebElement deleteButton;
	
	int currentCoffees=0;
	int newCurrentCoffees=0;
	int cont=0;
	
	boolean flag=false;
	
    @Given("^The user is on the home page_dc$")				
    public void the_user_is_on_the_home_page_anc() throws Throwable							
    {		   
        driver=openWebPage(driver);	      
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(),"Wecome to coffee demo.");
        wait(2);
        
    }		

    @When("^The user selects the coffee menu_dc$")					
    public void the_user_selects_the_coffee_menu_anc() throws Throwable 							
    {	
        driver.findElement(By.xpath("//a[@href='/coffee']")).click();
    }		

    @Then("^The current coffee products should be displayed_dc$")					
    public void the_current_coffee_products_should_be_displayed_anc() throws Throwable 							
    {    		
        wait = new WebDriverWait(driver, 65);
        deleteButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[2]")));
    	wait(1);
    	Assert.assertEquals(deleteButton.getText(),"Delete");
    	rows = driver.findElements(By.xpath("//table/tbody/tr"));
    	currentCoffees=rows.size();
    	
    }		
    
    @When("^The user selects the delete option for a coffee$")					
    public void the_user_selects_the_delete_option_for_a_coffee() throws Throwable 							
    {    		
        deleteButton.click();
        wait(1);
    }
    
    @Then("^That coffee should be deleted$")					
    public void that_coffee_should_be_deleted() throws Throwable 							
    {     	    	
    	
    	while(currentCoffees-1 != newCurrentCoffees & flag==false){   		
    		cont++;
    		
    		if(cont>125)
    			flag=true;
    		
    		wait(1);
    		rows = driver.findElements(By.xpath("//table/tbody/tr"));
    		newCurrentCoffees=rows.size();
    	}
    	
        Assert.assertEquals(newCurrentCoffees, currentCoffees-1);
    	driver.quit();
    }
        
}
