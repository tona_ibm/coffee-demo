package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class userMenuStepDefinition extends baseMethods{
 
	
	WebDriver driver; 
	
    @Given("^user is on the home page$")				
    public void user_is_on_the_home_page() throws Throwable							
    {		
      
        driver=openWebPage(driver);	      
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(),"Wecome to coffee demo.");
        wait(2);
        
    }		

    @When("^The user selects the user menu$")					
    public void the_user_selects_the_User_Menu() throws Throwable 							
    {	
        driver.findElement(By.xpath("//a[@href='/user']")).click();
    }		

    @Then("^The user should be in the user menu page$")					
    public void the_user_should_be_now_in_the_User_Menu_page() throws Throwable 							
    {    		
        
        Assert.assertEquals(driver.getCurrentUrl(),"http://localhost:8081/user");
        wait(1);
        driver.quit(); 
    }		
	
    
}

