package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class showCurrentCoffeeProductsStepDefinition extends baseMethods{
 
	
	WebDriver driver;
	WebDriverWait wait; 
	
    @Given("^The user is on the home page_scc$")				
    public void the_user_is_on_the_home_page_scc() throws Throwable							
    {		
      
        driver=openWebPage(driver);	      
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(),"Wecome to coffee demo.");
        wait(2);
        
    }		

    @When("^The user selects the coffee menu_scc$")					
    public void the_user_selects_the_coffee_menu_scc() throws Throwable 							
    {	
        driver.findElement(By.xpath("//a[@href='/coffee']")).click();
    }		

    @Then("^The current coffee products should be displayed$")					
    public void the_current_coffee_products_should_be_displayed() throws Throwable 							
    {    		
        wait = new WebDriverWait(driver, 65);
        WebElement editButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[1]")));
    	wait(1);    	
    	Assert.assertEquals(editButton.getText(),"Edit");
        driver.quit();
    }		
    
    
}
