package stepDefinitions;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class addNewCoffeeStepDefinition extends baseMethods{
 
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement editButton;
	List <WebElement> rows;
	
	int currentCoffees=0;
	int newCurrentCoffees=0;
	int cont=0;
	
	boolean flag=false;
	
    @Given("^The user is on the home page_anc$")				
    public void the_user_is_on_the_home_page_anc() throws Throwable							
    {		
      
        driver=openWebPage(driver);	      
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(),"Wecome to coffee demo.");
        wait(2);
        
    }		

    @When("^The user selects the coffee menu_anc$")					
    public void the_user_selects_the_coffee_menu_anc() throws Throwable 							
    {	
        driver.findElement(By.xpath("//a[@href='/coffee']")).click();
    }		

    @Then("^The current coffee products should be displayed_anc$")					
    public void the_current_coffee_products_should_be_displayed_anc() throws Throwable 							
    {    		
        wait = new WebDriverWait(driver, 65);
        editButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[1]")));
    	wait(1);
    	Assert.assertEquals(editButton.getText(),"Edit");
    	List <WebElement> rows = driver.findElements(By.xpath("//table/tbody/tr"));
    	currentCoffees=rows.size();
    	
    }		
    
    @When("^The user fills the information for a new coffee$")					
    public void the_user_fills_the_information_for_a_new_coffee() throws Throwable 							
    {    		
        driver.findElement(By.cssSelector("input[placeholder='Coffee name']")).sendKeys("Test_coffee");
        wait(1);
        driver.findElement(By.cssSelector("input[placeholder='Type']")).sendKeys("Cold");
        wait(1);
        driver.findElement(By.cssSelector("input[placeholder='Price']")).sendKeys("40");
        wait(1);
    }
    
    @And("^The user selects the submit option$")					
    public void the_user_selects_the_submit_option() throws Throwable 							
    {    		
        driver.findElement(By.cssSelector("button[type='submit']")).click();
        wait(1);
    }
    
    @Then("^The new coffee should be added$")					
    public void the_new_coffee_should_be_added() throws Throwable 							
    {   
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    	
    	while(currentCoffees+1 != newCurrentCoffees & flag==false){   		
    		cont++;
    		
    		if(cont>185)
    			flag=true;
    		
    		wait(1);
    		rows = driver.findElements(By.xpath("//table/tbody/tr"));
    		newCurrentCoffees=rows.size();
    	}
    	
    	Assert.assertEquals(newCurrentCoffees, currentCoffees+1);
    	driver.quit();
    }
        
}

