package stepDefinitions;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class addNewUserStepDefinition extends baseMethods{
 
	
	WebDriver driver;
	WebDriverWait wait;
	List <WebElement> rows;
	List <WebElement> pages;	
	WebElement pagination;
	WebElement editButton;
	
	int currentPages=0;
	int currentTableRows=0;
	int currentUsers=0;
	int newCurrentPages=0;
	int newCurrentUsers=0;
	int cont=0;
	
	boolean flag=false;
	
    @Given("^The user is on the home page_anu$")				
    public void the_user_is_on_the_home_page_anu() throws Throwable							
    {		
      
        driver=openWebPage(driver);	      
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(),"Wecome to coffee demo.");
        wait(2);
        
    }		

    @When("^The user selects the user menu_anu$")					
    public void the_user_selects_the_user_menu_anu() throws Throwable 							
    {	
        driver.findElement(By.xpath("//a[@href='/user']")).click();
    }		

    @Then("^The current users should be displayed_anu$")					
    public void the_current_users_should_be_displayed_anu() throws Throwable 							
    {    		
    	wait = new WebDriverWait(driver, 65);
        editButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[1]")));
        Assert.assertEquals(editButton.getText(), "Edit");
        
    	pagination = driver.findElement(By.xpath("/html/body/div/div/main/div[3]/ul"));
    	pages = pagination.findElements(By.tagName("li"));
    	currentPages=pages.size()-2;
    	wait(2);
    	driver.findElement(By.xpath("//a[@aria-label='Page "+currentPages+"']")).click();
    	
        editButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[1]")));
        Assert.assertEquals(editButton.getText(), "Edit");
    	
    	rows = driver.findElements(By.xpath("//table/tbody/tr"));
    	currentTableRows=rows.size();
    	currentUsers=((currentPages-1)*6)+currentTableRows;
    }		
    
    @When("^The user fills the information for a new user$")					
    public void the_user_fills_the_information_for_a_new_user() throws Throwable 							
    {    		
        driver.findElement(By.cssSelector("input[placeholder='Name']")).sendKeys("Daniel");
        wait(1);
        driver.findElement(By.cssSelector("input[placeholder='Last name']")).sendKeys("Casillas");
        wait(1);
        driver.findElement(By.cssSelector("input[placeholder='Email']")).sendKeys("test@test.com");
        wait(1);
    }
    
    @And("^The user selects the submit option_anu$")					
    public void the_user_selects_the_submit_option_anu() throws Throwable 							
    {    		
        driver.findElement(By.cssSelector("button[type='submit']")).click();
        wait(1);
    }
    
    @Then("^The new user should be added$")					
    public void the_new_user_should_be_added() throws Throwable 							
    {   
    	
    	while(currentUsers+1 != newCurrentUsers & flag==false){   		
    		cont++;
    		
    		if(cont>185)
    			flag=true;
    		
    		wait(1);
        	pagination = driver.findElement(By.xpath("/html/body/div/div/main/div[3]/ul"));
        	pages = pagination.findElements(By.tagName("li"));
        	newCurrentPages=pages.size()-2;
        
        	if(newCurrentPages>currentPages) {
        		driver.findElement(By.xpath("//a[@aria-label='Page "+newCurrentPages+"']")).click();
        		editButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody/tr[1]/td[5]/button[1]")));
                Assert.assertEquals(editButton.getText(), "Edit");
        	} 		    	
        	
        	rows = driver.findElements(By.xpath("//table/tbody/tr"));
        	currentTableRows=rows.size();
        	newCurrentUsers=((newCurrentPages-1)*6)+currentTableRows;		
    	}
    	
    	wait(2);
        Assert.assertEquals(newCurrentUsers, currentUsers+1);
    	driver.quit();
    	
    }
        
}
